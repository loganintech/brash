#![warn(clippy::pedantic)]
#![feature(drain_filter)]

mod commands;
mod process_pool;
mod segments;

use commands::cd::change_directory;
use segments::Segment;
use segments::{
    command_count::CommandCount,
    cwd::Directory,
    go_version::GoVersion,
    rust_version::RustVersion,
    user::{HostnameVerbosity, NameAndHostname},
};
use std::io::{self, BufRead, Write};

fn main() {
    let mut pool = process_pool::ProcessPool::new();
    // let current_path = std::env::current_dir().unwrap();

    let mut segments: Vec<Box<Segment>> = vec![
        Box::new(CommandCount::new(0)),
        Box::new(NameAndHostname::with_verbosity(HostnameVerbosity::UserHost)),
        Box::new(RustVersion),
        Box::new(GoVersion),
        Box::new(Directory),
    ];
    segments.sort_by_key(|f| f.order());

    loop {
        let command = prompt(segments.as_slice()).unwrap();
        let mut command_parts = command.split_whitespace();
        let command = command_parts.next().unwrap_or("");

        match command {
            "cd" => {
                if change_directory(command_parts.next()).is_none() {
                    eprint!("Couldn't change directory.");
                };
            }
            "jobs" => {
                output(&pool.jobs());
            }
            "exit" => break,
            "" => {}
            _ => {
                if let Err(e) = pool.add(command, command_parts.collect()) {
                    println!("Error adding command to process pool: {}", e);
                    io::stdout().flush().ok().expect("Could not flush stdout");
                };
            }
        }
    }
}

fn output(arg: &str) {
    if arg != "" {
        println!("{}", arg);
    }
}

fn prompt(segments: &[Box<Segment>]) -> Option<String> {
    let stdout = std::io::stdout();
    let mut stdout = stdout.lock();
    for segment in segments {
        if segment.should_display() {
            let _ = stdout.write_all(format!("[{}]", segment.text()).as_bytes());
        }
    }
    print!(": ");
    io::stdout().flush().ok().expect("Could not flush stdout");

    let mut buffer = String::new();
    let stdin = io::stdin();
    let mut handle = stdin.lock();

    //Read input from the user
    handle.read_line(&mut buffer).unwrap();

    buffer = buffer.replace("$$", &format!("{}", std::process::id()));

    //Trim buffer to take whitespace off of the right-side of a string
    let buffer = buffer.trim_end();

    Some(buffer.into())
}
