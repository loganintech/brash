#![warn(clippy::pedantic)]
#![feature(drain_filter)]

mod commands;
mod process_pool;
pub mod segments;

use commands::cd::change_directory;
use segments::Segment;
use std::io::{self, BufRead, Write};

use std::error::Error;
use std::result::Result;

pub struct Prompt {
    segments: Vec<Box<Segment>>,
    pool: process_pool::ProcessPool,
}

impl Prompt {
    pub fn new() -> Self {
        Prompt {
            segments: vec![],
            pool: process_pool::ProcessPool::new(),
        }
    }

    pub fn with_segment<T: Segment + 'static>(mut self, segment: T) -> Self {
        self.segments.push(Box::new(segment));
        self
    }

    pub fn run(&mut self) -> Result<(), Box<dyn Error>> {
        self.segments.sort_by_key(|s| s.order());

        loop {
            let command = prompt(self.segments.as_slice()).unwrap();
            let mut command_parts = command.split_whitespace();
            let command = command_parts.next().unwrap_or("");

            match command {
                "cd" => {
                    if let None = change_directory(command_parts.next()) {
                        eprint!("Couldn't change directory.");
                    };
                }
                "jobs" => {
                    output(&self.pool.jobs());
                }
                "exit" => break,
                _ => {
                    if let Err(e) = self.pool.add(command, command_parts.collect()) {
                        eprintln!("Error adding command to process pool: {}", e);
                    };
                }
            }
        }
        Ok(())
    }
}

fn output(arg: &str) {
    if arg != "" {
        println!("{}", arg);
    }
}

fn prompt(segments: &[Box<Segment>]) -> Option<String> {
    let stdout = std::io::stdout();
    let mut stdout = stdout.lock();
    for segment in segments {
        if segment.should_display() {
            let _ = stdout.write_all(format!("[{}]", segment.text()).as_bytes());
        }
    }
    print!(": ");
    io::stdout().flush().ok().expect("Could not flush stdout");

    let mut buffer = String::new();
    let stdin = io::stdin();
    let mut handle = stdin.lock();

    //Read input from the user
    handle.read_line(&mut buffer).unwrap();

    buffer = buffer.replace("$$", &format!("{}", std::process::id()));

    //Trim buffer to take whitespace off of the right-side of a string
    let buffer = buffer.trim_end();

    Some(buffer.into())
}
