use super::{Segment, SegmentOrder};

pub struct DebugSegment;

impl Segment for DebugSegment {
    // #[cfg(debug_assertions)]
    fn should_display(&self) -> bool {
        true
    }

    // #[cfg(not(debug_assertions))]
    // fn should_display(&self) -> bool {
    // false
    // }

    fn text(&self) -> String {
        String::from("Debug Mode Enabled")
    }

    fn order(&self) -> SegmentOrder {
        //We want to give this a high priority so that if something new is added it will be moved right until we set a place for it
        SegmentOrder::Left(0)
    }
}
