use super::{Segment, SegmentOrder};
use std::path::PathBuf;

pub struct GoVersion;

impl Segment for GoVersion {
    fn should_display(&self) -> bool {
        let path = std::env::current_dir().unwrap_or_else(|_| PathBuf::new());

        let gopath = PathBuf::from(
            std::env::var_os("GOPATH")
                .and_then(|pth: std::ffi::OsString| pth.into_string().ok())
                .unwrap_or_else(|| "".to_string()),
        );
        path.starts_with(gopath)
    }

    fn text(&self) -> String {
        use std::io;
        use std::process::{Command, Output};

        let output = Command::new("go").arg("version").output();

        match {
            if let Ok(Output { stdout, .. }) = output {
                String::from_utf8(stdout).map_err(|_| {
                    io::Error::new(io::ErrorKind::InvalidData, "Couldn't read rustc output")
                })
            } else {
                Err(io::Error::new(
                    io::ErrorKind::NotFound,
                    "Couldn't get rustc output",
                ))
            }
        } {
            Ok(out) => out.split(' ').nth(2).unwrap_or_else(|| "")[2..].to_string(),
            _ => String::new(),
        }
    }

    fn order(&self) -> SegmentOrder {
        SegmentOrder::Right(2)
    }
}
