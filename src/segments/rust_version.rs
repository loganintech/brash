use super::{Segment, SegmentOrder};
use std::path::PathBuf;

pub struct RustVersion;

impl Segment for RustVersion {
    fn should_display(&self) -> bool {
        let path = std::env::current_dir().unwrap_or_else(|_| PathBuf::new());

        path.ancestors()
            .any(|path| path.join("Cargo.toml").exists())
    }

    fn text(&self) -> String {
        use std::io;
        use std::process::{Command, Output};

        let output = Command::new("rustc").arg("--version").output();

        match {
            if let Ok(Output { stdout, .. }) = output {
                String::from_utf8(stdout).map_err(|_| {
                    io::Error::new(io::ErrorKind::InvalidData, "Couldn't read rustc output")
                })
            } else {
                Err(io::Error::new(
                    io::ErrorKind::NotFound,
                    "Couldn't get rustc output",
                ))
            }
        } {
            Ok(out) => out.split(' ').nth(1).unwrap_or_else(|| "").to_string(),
            _ => String::new(),
        }
    }

    fn order(&self) -> SegmentOrder {
        SegmentOrder::Right(1)
    }
}
