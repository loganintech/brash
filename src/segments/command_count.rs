use super::{Segment, SegmentOrder};
use std::cell::RefCell;

pub struct CommandCount(RefCell<usize>);

impl CommandCount {
    pub fn new(count: usize) -> Self {
        Self(RefCell::new(count))
    }
}

impl Segment for CommandCount {
    fn should_display(&self) -> bool {
        true
    }

    fn text(&self) -> String {
        *self.0.borrow_mut() += 1;
        format!("{}", *self.0.borrow() - 1)
    }

    fn order(&self) -> SegmentOrder {
        SegmentOrder::Left(0)
    }
}
