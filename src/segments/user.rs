use super::{Segment, SegmentOrder};
use whoami;

#[allow(dead_code)]
pub enum HostnameVerbosity {
    User,
    UserHost,
    UserHostOs,
}

pub struct NameAndHostname {
    verbosity: HostnameVerbosity,
}

impl NameAndHostname {
    pub fn with_verbosity(verbosity: HostnameVerbosity) -> Self {
        Self { verbosity }
    }
}

impl Segment for NameAndHostname {
    fn should_display(&self) -> bool {
        true
    }

    fn text(&self) -> String {
        use HostnameVerbosity::*;
        match self.verbosity {
            User => whoami::username(),
            UserHost => format!("{}@{}", whoami::username(), whoami::hostname()),
            UserHostOs => format!(
                "{}@{} ({})",
                whoami::username(),
                whoami::hostname(),
                whoami::os()
            ),
        }
    }

    fn order(&self) -> SegmentOrder {
        SegmentOrder::Left(2)
    }
}
