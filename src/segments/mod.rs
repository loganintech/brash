pub mod command_count;
pub mod cwd;
pub mod debug_segment;
pub mod go_version;
pub mod rust_version;
pub mod user;
use std::cmp::Ordering;

#[derive(PartialEq, Eq)]
pub enum SegmentOrder {
    Left(u32),
    Right(u32),
}

pub trait Segment {
    fn should_display(&self) -> bool {
        true
    }

    fn text(&self) -> String;

    fn order(&self) -> SegmentOrder {
        SegmentOrder::Left(1000)
    }
}

impl PartialOrd for SegmentOrder {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for SegmentOrder {
    fn cmp(&self, other: &Self) -> Ordering {
        use SegmentOrder::*;
        match (self, other) {
            (Left(_), Right(_)) => Ordering::Less,
            (Right(_), Left(_)) => Ordering::Greater,
            (Left(x), Left(y)) | (Right(x), Right(y)) => x.cmp(&y),
        }
    }
}
