use super::{Segment, SegmentOrder};
use std::env;

pub struct Directory;

impl Segment for Directory {
    fn should_display(&self) -> bool {
        true
    }

    fn text(&self) -> String {
       format!("{}", env::current_dir().unwrap().display())
    }

    fn order(&self) -> SegmentOrder {
        SegmentOrder::Left(3)
    }
}
