use dirs::home_dir;
use std::env;
use std::path::PathBuf;

pub fn change_directory(dir: Option<&str>) -> Option<PathBuf> {
    let mut current: PathBuf = env::current_dir().unwrap();

    let gotodir = |dr| match env::set_current_dir(&dr) {
        Ok(()) => Some(dr),
        _ => None,
    };

    match dir {
        Some("..") => {
            current.pop();
            gotodir(current)
        }
        Some(".") => Some(current),
        Some("~") => gotodir(home_dir().unwrap()),
        Some(dir) => {
            current.push(dir);
            gotodir(current)
        }
        None => gotodir(home_dir().unwrap()),
    }
}
